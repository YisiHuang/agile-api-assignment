# Assignment 2 - Agile Software Practice.

Name: Yisi Huang

## API endpoints.

movies:
- GET /api/movies - Get movies from tmdb 
- POST /api/movies - Add movie into movie list
- GET /api/movies/tmdb/movie/:id/reviews - Get movie reviews for a movie
- GET /api/movies/:id - Get movie details for a movie
- GET /api/movies/tmdb/movie/:id/images - Get movie images for a movie
- GET /api/movies/tmdb/discover - Get a list of movies at homepage
- GET /api/movies/tmdb/upcoming - Get a list of upcoming movies
- GET /api/movies/tmdb/topRated - Get a list of top rated
- GET /api/movies/tmdb/nowPlaying - Get a list of now playing movies
- GET /api/movies/tmdb/trending - Get a list of trending movies

genres:
- GET /api/genres/tmdb/genres - Get movie genres

users:
- GET /api/users - Get user information
- POST /api/users(Auth) - Add a new user

people:
- GET /api/people/movie/:id/credits - Get movie credits for a movie
- GET /api/people/:id - Get people details of movies

## Automated Testing.

~~~
  Users endpoint
    GET /api/users
      √ should return the 2 users and a status 200 (80ms)
    POST /api/users
      √ should register a new user (320ms)
      √ should return an error for missing username or password
    For an authenticate action
      √ should authenticate an existing user (341ms)
      √ should return an error for invalid credentials (413ms)

  Movies endpoint
    GET /api/movies/tmdb/upcoming
      √ should return a list of upcoming movies (341ms)
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie (96ms)
      when the id is invalid
        √ should return the NOT found message (59ms)
    GET /api/movies/tmdb/trending
      √ should return a list of trending movies (166ms)
    GET /api/movies/tmdb/nowPlaying
      √ should return a list of now playing movies (229ms)
    GET /api/movies/tmdb/topRated
      √ should return a list of top rated TV shows (265ms)
    GET /api/movies/tmdb/movie/:id
      √ should return the details for a movie (245ms)
    GET /api/movies/tmdb/discover
      √ should return a list of homepage movies (228ms)
    GET /api/movies/tmdb/movie/:id/reviews
      √ should return the reviews for a movie (432ms)
    GET /api/movies/tmdb/movie/:id/images
      √ should return images for a movie (293ms)

  People endpoint
    GET /api/people/movie/:id/credits
      √ should return an array of cast, an array of crew and a status 200 (1509ms)
      √ should return the same credits pulled from TMDB API (1184ms)
      √ should update the DB (1372ms)
    GET /api/people/:id
      when the id is valid
        √ should return the matching person (432ms)
        √ should return the same movie pulled from TMDB API (557ms)
        √ should update the DB (434ms)
      when the id is invalid
        √ should return an error message (294ms)
        √ should not update the DB (288ms)

  Genres endpoint
    GET /api/genres/tmdb/genres
      √ should return an array of genres (221ms)
      √ should return the same genres pulled from TMDB API (165ms)

  25 passing (22s)
~~~


## Deployments.

+ Movies
- Get a list of movies: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies
- Get a list of movies at homepage: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/discover
- Get a list of upcoming movies: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/upcoming
- Get a list of top rated tvs: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/topRated
- Get a list of trending movies: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/trending
- Get the movie information of movie id 671583: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/671583
- Get a list of now playing movies: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/nowPlaying
- Get the movie details of movie id 848326: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/movie/848326
- Get the movie reviews of movie id 848326: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/movie/848326/reviews
- Get the movie images of movie id 848326: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/movies/tmdb/movie/848326/images

+ Genres
- Get a list of genres for movies: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/genres/tmdb/genres

+ Users
- Get a list of users: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/users

+ People
- Get a list of credits for movie id 848326: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/people/movie/848326/credits
- Get people details for people id 91520: https://movies-api-staging-hys-b3b054de69a8.herokuapp.com/api/people/91520

## Independent Learning (if relevant)

https://coveralls.io/gitlab/YisiHuang/agile-api-assignment

I learnt how to generate test reports with coveralls, the most important point is to add the coveralls token value into the gitlab CI/CD variable and also the yaml file has to be changed so that the test reports are automatically transferred to the coveralls after the pipeline passes.
The following configuration commands are used:
~~~
npm install coveralls --save-dev
npm install --save-dev nyc
npm test
~~~

## Gitlab Link
https://gitlab.com/YisiHuang/agile-api-assignment.git