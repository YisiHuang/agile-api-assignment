import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test123@",
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test123@",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
          done();
        });
    });
  });

  describe("POST /api/users", () => {
    it("should register a new user", async () => {
      const newUser = {
        username: "newuser",
        password: "test123@"
      };
      const res = await request(api)
        .post('/api/users')
        .query({ action: 'register' })
        .send(newUser)
        .expect(201);

      expect(res.body).to.include.keys('msg', 'success');
    });

    it("should return an error for missing username or password", async () => {
      const res = await request(api)
        .post('/api/users')
        .query({ action: 'register' })
        .send({ username: "newuser" }) // Missing password
        .expect(400);

      expect(res.body).to.have.property('msg', 'Username and password are required.');
    });
  });

  describe("For an authenticate action", () => {
    it("should authenticate an existing user", async () => {
      const userCredentials = {
        username: "user1",
        password: "test123@" 
      };

      const res = await request(api)
        .post('/api/users')
        .send(userCredentials)
        .expect(200);

      expect(res.body).to.have.property('success', true);
    });

    it("should return an error for invalid credentials", async () => {
      const invalidCredentials = {
        username: "user1",
        password: "wrongpassword"
      };

      const res = await request(api)
        .post('/api/users')
        .send(invalidCredentials)
        .expect(401); 

      expect(res.body).to.have.property('msg', 'Wrong password.'); 
    });
  });
});
