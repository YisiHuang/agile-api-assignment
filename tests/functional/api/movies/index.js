import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("GET /api/movies/tmdb/upcoming", () => {
    it("should return a list of upcoming movies", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/upcoming`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.a("object");
      if (res.body.length > 0) {
        res.body.forEach(movie => {
          expect(movie).to.include.keys('adult', 'backdrop_path', 'genre_ids', 'id', 
                                        'original_language', 'original_title', 'overview', 
                                        'popularity', 'poster_path', 'release_date', 
                                        'title', 'video', 'vote_average', 'vote_count');
          expect(movie.genre_ids).to.be.an('array');
        });
      }
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    });

    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .expect(404)
          .expect({
            status_code: 404,
            message: "The movie you requested could not be found.",
          });
      });
    });
  });

  describe("GET /api/movies/tmdb/trending", () => {
    it("should return a list of trending movies", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/trending`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.a("object");
      if (res.body.length > 0) {
        res.body.forEach(movie => {
          expect(movie).to.include.keys('adult', 'backdrop_path', 'genre_ids', 'id', 
                                        'original_language', 'original_title', 'overview', 
                                        'popularity', 'poster_path', 'release_date', 
                                        'title', 'video', 'vote_average', 'vote_count');
          expect(movie.genre_ids).to.be.an('array');
        });
      }
    });
  });

  describe("GET /api/movies/tmdb/nowPlaying", () => {
    it("should return a list of now playing movies", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/nowPlaying`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.a("object");
      if (res.body.length > 0) {
        res.body.forEach(movie => {
          expect(movie).to.include.keys('adult', 'backdrop_path', 'genre_ids', 'id', 
                                        'original_language', 'original_title', 'overview', 
                                        'popularity', 'poster_path', 'release_date', 
                                        'title', 'video', 'vote_average', 'vote_count');
          expect(movie.genre_ids).to.be.an('array');
        });
      }
    });
  });

  describe("GET /api/movies/tmdb/topRated", () => {
    it("should return a list of top rated TV shows", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/topRated`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.a("object");
      if (res.body.length > 0) {
        res.body.forEach(show => {
          expect(show).to.include.keys('adult', 'backdrop_path', 'genre_ids', 'id', 
                                        'origin_country', 'original_language', 'original_name', 
                                        'overview', 'popularity', 'poster_path', 'first_air_date', 
                                        'name', 'vote_average', 'vote_count');
          expect(show.genre_ids).to.be.an('array');
          expect(show.origin_country).to.be.an('array');
        });
      }
    });
  });

  describe("GET /api/movies/tmdb/movie/:id", () => {
    it("should return the details for a movie", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/movie/${movies[0].id}`)
        .set("Accept", "application/json")
        .expect(200);
      
      expect(res.body).to.be.a('object');
      expect(res.body).to.have.property("id", movies[0].id);
    });
  });

  describe("GET /api/movies/tmdb/discover", () => {
    it("should return a list of homepage movies", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/discover`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.a("object");
      if (res.body.length > 0) {
        res.body.forEach(movie => {
          expect(movie).to.include.keys('adult', 'backdrop_path', 'genre_ids', 'id', 
                                        'original_language', 'original_title', 'overview', 
                                        'popularity', 'poster_path', 'release_date', 
                                        'title', 'video', 'vote_average', 'vote_count');
          expect(movie.genre_ids).to.be.an('array');
        });
      }
    });
  });

  describe("GET /api/movies/tmdb/movie/:id/reviews", () => {
    it("should return the reviews for a movie", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/movie/${movies[0].id}/reviews`)
        .set("Accept", "application/json")
        .expect(200)
        expect(res.body).to.be.a('array');
        res.body.forEach(review => {
          expect(review).to.include.all.keys('author', 'author_details', 'content',
           'created_at', 'id', 'updated_at', 'url');
          expect(review.author_details).to.be.an('object');
          expect(review.author_details).to.include.all.keys('name', 'username', 'avatar_path', 'rating');
        });
    });
  });

  describe("GET /api/movies/tmdb/movie/:id/images", () => {
    it("should return images for a movie", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/movie/${movies[0].id}/images`)
        .set("Accept", "application/json")
        .expect(200);

      expect(res.body).to.be.an("object");
      expect(res.body).to.have.property("backdrops").that.is.an("array");

      if (res.body.backdrops.length > 0) {
        res.body.backdrops.forEach(backdrop => {
          expect(backdrop).to.include.keys('aspect_ratio', 'height', 'iso_639_1', 'file_path', 'vote_average', 'vote_count', 'width');
        });
      }
    });
  });
});
